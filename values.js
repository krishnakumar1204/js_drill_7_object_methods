const keys = require("./keys");

function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    

    if (typeof obj == 'object' && !Array.isArray(obj) && obj != null) {

        let valueArray = [];

        for (let key in obj) {

            if (obj.hasOwnProperty(key)) {

                valueArray.push(obj[key]);
            }
        }

        return valueArray;
    }
    else {
        console.log("Input should be a valid Object");
    }
}


module.exports = values;