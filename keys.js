function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys

    if (typeof obj == 'object' && !Array.isArray(obj) && obj != null) {

        let keyArray = [];

        for (let key in obj) {

            if (obj.hasOwnProperty(key)) {
                
                keyArray.push(key);
            }
        }

        return keyArray;

    }
    else {
        console.log("Input should be a valid object.");
    }
}


module.exports = keys;