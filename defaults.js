function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    if (typeof obj == 'object' && !Array.isArray(obj) && obj != null) {

        for (key in obj) {

            if (obj.hasOwnProperty(key)) {

                if (obj[key] == undefined) {
                    
                    obj[key] = defaultProps;
                }
            }
        }
        return obj;
    }
    else {
        console.log("Input should be a valid object.");
    }
}

module.exports = defaults;