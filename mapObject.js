function mapObject(obj, cb) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject

    if (typeof obj == 'object' && !Array.isArray(obj) && obj != null) {

        let resultedObject = {};

        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                resultedObject[key] = cb(obj[key]);
            }
        }

        return resultedObject;
    }
    else {
        console.log("Input should be a valid object.");
    }
}


module.exports = mapObject;