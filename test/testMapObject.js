const testObject = require("../data");
const mapObject = require("../mapObject");

try {
    let modifiedObject = mapObject(testObject, (value) => {
        return "Modified " + value;
    });

    if (typeof modifiedObject == 'object' && !Array.isArray(modifiedObject) && modifiedObject != null) {

        console.log(modifiedObject);
    }

} catch (error) {
    console.log("Something went wrong");
}