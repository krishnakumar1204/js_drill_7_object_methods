const testObject = require("../data");
const invert = require("../invert");

try {
    let copyObject = invert(testObject);

    if (typeof copyObject == 'object' && !Array.isArray(copyObject) && copyObject != null) {

        console.log(copyObject);
    }

} catch (error) {
    console.log("Something went wrong");
}