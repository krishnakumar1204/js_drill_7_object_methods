const testObject = require("../data");
const pairs = require("../pairs");

try {
    let pairArray = pairs(testObject);
    if (Array.isArray(pairArray)) {
        console.log(pairArray);
    }
} catch (error) {
    console.log("Something went wrong");
}