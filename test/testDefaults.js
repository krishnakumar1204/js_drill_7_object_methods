const testObject = require("../data");
const defaults = require("../defaults");

try {
    let updatedObject = defaults(testObject, 'Bat Man');

    if (typeof updatedObject == 'object' && !Array.isArray(updatedObject) && updatedObject != null) {

        console.log(updatedObject);
    }
} catch (error) {
    console.log("Somethong went wrong");
}