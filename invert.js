function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    // Assume that all of the object's values will be unique and string serializable.
    // http://underscorejs.org/#invert

    if (typeof obj == 'object' && !Array.isArray(obj) && obj != null) {

        let copyObject = {};

        for (let key in obj) {

            if (obj.hasOwnProperty(key)) {

                copyObject[obj[key]] = key;
            }
        }
        return copyObject;
    }
    else {
        console.log("Input should be a valid Object.");
    }
}

module.exports = invert;