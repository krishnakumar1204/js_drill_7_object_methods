function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    if (typeof obj == 'object' && !Array.isArray(obj) && obj != null) {

        let pairArray = [];

        for (let key in obj) {

            if (obj.hasOwnProperty(key)) {
                
                pairArray.push([key, obj[key]]);
            }
        }
        return pairArray;
    }
    else {
        console.log("Input should be a valid object");
    }
}


module.exports = pairs;